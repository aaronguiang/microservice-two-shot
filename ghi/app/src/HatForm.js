import React from 'react';


//compare with ConferenceForm
class HatForm extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            style_name: '',
            color: '',
            fabric: '',
            picture_url: '',
            location: '',
            locations: [],
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeStyle = this.handleChangeStyle.bind(this);
        this.handleChangeColor = this.handleChangeColor.bind(this);
        this.handleChangeFabric = this.handleChangeFabric.bind(this);
        this.handleChangePicture = this.handleChangePicture.bind(this);
        this.handleChangeLocation = this.handleChangeLocation.bind(this);
      }
    async componentDidMount() {
        const url = "http://localhost:8100/api/locations/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({locations: data.locations});
            }
        }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.locations
       // delete data.style_name
        //delete data.color
        //delete data.fabric
        //delete data.picture_url;

        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
                },
        };

        const response = await fetch(hatUrl, fetchConfig);

        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);
            // resets all fields to empty
            this.setState({
                style_name: '',
                color: '',
                fabric: '',
                picture_url: '',
                location: '',
            });
        }
    }

    handleChangeStyle(event) {
        const value = event.target.value;
        this.setState({style_name: value})
        }

    handleChangeColor(event) {
        const value = event.target.value;
        this.setState({color: value})
        }

    handleChangeFabric(event) {
        const value = event.target.value;
        this.setState({fabric: value})
        }

    handleChangePicture(event) {
        const value = event.target.value;
        this.setState({picture_url: value})
        }

    handleChangeLocation(event) {
        const value = event.target.value;
        this.setState({location: value})
        }
    render() {
        return (
            <React.Fragment>
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>Create New Hat</h1>
                            <form onSubmit={this.handleSubmit} id="create-hat-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangeStyle} value={this.state.style_name} placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control"/>
                                <label htmlFor="style_name">Style Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangeColor} value={this.state.color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                            <input onChange={this.handleChangeFabric} value={this.state.fabric} placeholder="fabric" required
                                type="text" name="fabric" id="fabric"
                                className="form-control" />
                                <label htmlFor="fabric">Fabric</label>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="picture_url" className ="form-label">Picture</label>
                                <input onChange={this.handleChangePicture} value={this.state.hatUrl}  name="picture_url" required type="url" id="picture_url" className="form-control" rows="3"></input>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleChangeLocation} value={this.state.location} required id="location" name="location" className="form-select">
                                <option value="">Location</option>
                                {this.state.locations.map(location => {
                            
                                return (
                                    <option key={location.id} value={location.href}>{location.closet_name}</option>
                                  );
                                })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
              </React.Fragment>
            );
          }
}

export default HatForm;
