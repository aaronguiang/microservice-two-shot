import React, { useEffect, useState } from 'react'

function ShoeForm() {
  // show dropdown of all possible bins:
  const [bins, setBins] = useState([]);

  // when someone selects a specific bin:
  const [bin, setBin] = useState('');

  const [modelName, setModelName] = useState('');
  const [manufacturer, setManufacturer] = useState('');
  const [color, setColor] = useState('');
  const [picture, setPicture] = useState('');

  const handleModelNameChange = (event) => {
    const value = event.target.value;
    setModelName(value);
  }

  const handleManufacturerChange   = (event) => {
    const value = event.target.value;
    setManufacturer(value);
  }

  const handleColorChange = (event) => {
    const value = event.target.value;
    setColor(value);
  }

  const handlePictureChange = (event) => {
    const value = event.target.value;
    setPicture(value);
  }

  // when someone selects a bin we change the state
  const handleBinChange = (event) => {
    const value = event.target.value;
    setBin(value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.bin = bin;
    data.model_name = modelName;
    data.manufacturer = manufacturer;
    data.color = color;
    data.picture_url = picture;

    const shoeUrl = 'http://localhost:8080/api/shoes/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    console.log(data)
    const response = await fetch(shoeUrl, fetchConfig);
    if (response.ok) {
      const newShoe = await response.json();
      console.log(newShoe);
      setModelName('');
      setBin('');
      setPicture('');
      setColor('');
      setManufacturer('');
    }

  }

  const fetchBins = async () => {
    const url = 'http://localhost:8100/api/bins/'

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setBins(data.bins)
    }
  }

  useEffect(() => {
    fetchBins();
  }, []);

  return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new Shoe</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input onChange={handleModelNameChange} value={modelName} placeholder="Model name" required type="text" name="model_name" id="model_name" className="form-control"/>
                <label htmlFor="model_name">Shoe Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleManufacturerChange} value={manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePictureChange} value={picture} placeholder="Picture" type="text" name="picture_url" id="picture_url" className="form-control"/>
                <label htmlFor="picture_url">Picture</label>
              </div>
              <div className="mb-3">
                <select onChange={handleBinChange} value={bin} required name="bin" id="bin" className="form-select">
                  <option defaultValue={''} value="">Choose A Bin</option>
                  {bins.map(bin => {
                    return (
                      <option key={bin.id} value={bin.id}>
                        {bin.id}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
  );
}

export default ShoeForm;
