import React, { useEffect, useState } from 'react'

function ShoeList(props) {
  const [shoes, setShoes] = useState([]);


  const handleDeleteShoe = async (event) => {
    const shoeId = event.target.value;
    const deleteUrl = `http://localhost:8080/api/shoes/${shoeId}/`;

    const fetchConfig = {
      method: "delete",
    };

    const response = await fetch (deleteUrl, fetchConfig);
    loadShoes();
  }

  async function loadShoes() {
    const response = await fetch('http://localhost:8080/api/shoes/');

    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes);

    } else {
      console.error(response)
    }
  }

  useEffect(() => {
    loadShoes();
  }, []);

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>
            Id
          </th>
          <th>
            Shoe Model Name
          </th>
          <th>
            Manufacturer
          </th>
          <th>
            Bin
          </th>
          <th>
            Color
          </th>
          <th>
            Picture
          </th>
          <th>
            Delete
          </th>
        </tr>
      </thead>
      <tbody>
        {shoes.map(shoe => {
          return (
            <tr key={shoe.id}>
              <td>{shoe.id}</td>
              <td>{shoe.model_name}</td>
              <td>{shoe.manufacturer}</td>
              <td>{shoe.bin.closet_name}</td>
              <td>{shoe.color}</td>
              <td>{shoe.picture_url}</td>
              <td>
                <button onClick={handleDeleteShoe} value={shoe.id} className="btn btn-danger btn-sm">Delete</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );

}

export default ShoeList;
