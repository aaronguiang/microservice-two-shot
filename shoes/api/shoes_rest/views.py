from django.shortcuts import render
from .models import Shoes, BinVO
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder

class BinVOListEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "import_href",
    ]


class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "picture_url",
        "color",
        "id",
        "model_name",
        "manufacturer",
        "bin"
    ]
    encoders = {
        "bin": BinVOListEncoder(),
    }



class ShoeDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "id",
        "model_name",
        "color",
        "manufacturer",
        "picture_url",
        "bin",
    ]
    encoders = {
        "bin": BinVOListEncoder(),
    }


@require_http_methods(["GET", "POST"])
def list_shoes(request):
    if request.method == "GET":
        print("hello world")
        shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesListEncoder,
        )

    else:
        content = json.loads(request.body)

        # we're going to get the Shoe object and put it in the content dict
        try:
            bin = BinVO.objects.get(import_href=f'/api/bins/{content["bin"]}/')
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        shoe = Shoes.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoesListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def show_shoes(request, id):
    if request.method == "GET":
        shoe = Shoes.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Shoes.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "bin" in content:
                # we're searching the BinVOs table below to find a row
                # that has our bin href string in the import_href column
                bin = BinVO.objects.get(import_href=f'/api/bins/{content["bin"]}/')
                content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        Shoes.objects.filter(id=id).update(**content)
        shoe = Shoes.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
