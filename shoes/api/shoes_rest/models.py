from django.db import models
from django.urls import reverse

class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    import_href = models.CharField(max_length=200, unique=True)


class Shoes(models.Model):
    manufacturer = models.CharField(max_length=40)
    color = models.CharField(max_length=40)
    model_name = models.CharField(max_length=40)
    picture_url = models.URLField(null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
    )

    # def get_api_url(self):
    #     return reverse("list_shoes", kwargs={"pk": self.pk})

    # def __str__(self):
    #     return self.name

    class Meta:
        ordering = ("model_name",)  # Default ordering for Shoes
