from django.http import JsonResponse
from django.shortcuts import render
from .models import Hats, LocationVO
import json
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
# Create your views here.


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        'import_href',
        "closet_name",
        "section_number",
        "shelf_number",
    ]


class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "id",
        "style_name",
        "color",
        "fabric",
        "location",
        "picture_url",
    ]
    encoders = {
        'location' : LocationVODetailEncoder()
    }

class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "id",
        "style_name",
        "color",
        "fabric",
        "location",
        "picture_url",
    ]
    encoders = {
        'location' : LocationVODetailEncoder()
    }


@require_http_methods(["GET", "POST"])
def hat_list(request):
    # If the request method is GET, get all hats & return as a JSON response.
    if request.method == "GET":
            hats = Hats.objects.all()
            return JsonResponse(
                {"hats" : hats},
                encoder=HatsListEncoder,
            )
    #If POST, create a new hat object from the request body & return it as a JSON response.
    else:
        content = json.loads(request.body)
        ## Try to get the location from the request body & update content with corresponding LocationVO object.
        try:
            location_href = content['location']
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        # Create a new hat object using the request body and return it as a JSON response.
        hats = Hats.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatsDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def hat_detail(request,pk):
    if request.method == "GET":
        #Need to get hat obj with given "pk"
        hat = Hats.objects.get(id=pk)
        #returning JSON response with hat onj, encoded
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False,
        )
    #if not get, it'll be delete
    else:
        #delete with given pk
        count, _ = Hats.objects.filter(id=pk).delete()
        #return JSON response indicating if deleted
        return JsonResponse({"deleted": count > 0})
