from django.db import models

# Create your models here.

class LocationVO(models.Model):
    import_href = models.CharField(max_length=250, blank=True, null=True, unique=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField(blank=True, null=True)
    shelf_number = models.PositiveSmallIntegerField(blank=True, null=True)

    def __str__(self):
        return f"{self.closet_name} {self.section_number} {self.shelf_number}"



class Hats(models.Model):
    style_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    fabric = models.CharField(max_length=100)
    picture_url = models.URLField()
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
    )
